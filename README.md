# The Rain Intermediate Representation

This repository contains the formal documentation for the `rain` intermediate representation project, as well as technical implementation documents.