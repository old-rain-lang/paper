fn branched_double_use(b: bool, v: Vec<u8>) -> Vec<u8> {
    if b {
        double_vec(v)
    } else {
        triple_vec(v)
    }
}