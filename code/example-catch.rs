use rain::{Abort, fmap, catch_unwind, EResult, Exception, Panic};

fn checked_squared(data: Data) -> EResult<T, Abort> {
    fmap(
        // Note that a closure is unnecessary here, since 
        // exceptions are handled by ordinary types!
        catch_unwind(squared(data)), 
        |data| match data {
            Ok(data) => data,
            Err(_) => Data::invalid)
        }
    }
}