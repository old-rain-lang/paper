/// This code triples it's input vector
fn triple_vec(v: Vec<u8>) -> Vec<u8>;

fn double_use(v: Vec<u8>) -> (Vec<u8>, Vec<u8>) {
    (double_vec(v), triple_vec(v))
}