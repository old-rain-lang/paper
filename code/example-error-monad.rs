fn square_squared(data: Data) -> Data {
    square(square(data))
}

fn checked_squared(data: Data) -> Data {
    match std::panic::catch_unwind(|| square(data)) {
        Ok(data) => data,
        Err(_) => Data::invalid()
    }
}