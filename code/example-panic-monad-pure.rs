use rain::{bind, EResult, Panic};

fn square_squared(data: Data) -> EResult<Data, Panic> {
    bind(square(data), square)
}