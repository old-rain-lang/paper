use rain::{EResult, Panic, unwrap_error};

fn unwrap(opt_data: Option<Data>) -> EResult<Data, Panic> {
    match opt_data {
        Some(data) => EResult::ret(data),
        None => EResult::throw(
            unwrap_error!(line!(), column!(), file!())
        )
    }
}

fn square(data: Data) -> EResult<Data, Panic> {
    unwrap(compute(data, data))
}