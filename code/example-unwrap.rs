/// Some expensive computation, which is expected to return
/// `Some` if `left == right`
fn compute(left: Data, right: Data) -> Option<Data>;

fn square(data: Data) -> Data {
    compute(data, data).unwrap()
}