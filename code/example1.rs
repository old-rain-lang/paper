/// This function hashes it's argument
fn hash(s: &[u8]) -> u64;

/// This function doubles the value of it's argument
fn double_vec(v: Vec<u8>) -> Vec<u8>;

fn example_1(v: Vec<u8>) -> (Vec<u8>, u64) {
    let s: &[u8] = f(&v);
    let c: u64 = hash(s);
    let v = double_vec(v);
    (v, c)
}
