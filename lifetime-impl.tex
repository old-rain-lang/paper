\documentclass{article}
\usepackage{rain-lang}
\addbibresource{references.bib}

\title{Implementing \rain Lifetimes}
\author{Jad Elkhaleq Ghalayini}

\begin{document}

\maketitle

\section*{Abstract}

This is a technical document detailing the current implementation plans for \rain's substructural typing and lifetime systems, intended both as documentation and specification. An understanding of Rust lifetimes, substructural type systems, and \rain's semantics is assumed. We describe the \rain lifetime system as designed: a layer of stronger and stronger abstractions to achieve finer and finer low-level control of IO, memory and resource usage while retaining the characteristics of a purely functional language. From basic affine and relevant lifetimes, we successively extend the system with field lifetimes, cellular lifetimes, and finally abstract heaps, the last allowing the checked direct manipulation of pointers in a random access memory model as a minimal level of abstraction. Accompanying the formal description of each level (sections \ref{sec:basic}, \ref{sec:field}, \ref{sec:cellular}, \ref{sec:heap}) is a description of it's implementation in the \texttt{rain-ir} module (sections \ref{sec:basic-impl}, \ref{sec:field-impl}, \ref{sec:cellular-impl}, \ref{sec:heap-impl}), common usage patterns (sections \ref{sec:basic-pattern}, \ref{sec:field-pattern}, \ref{sec:cellular-pattern}, \ref{sec:heap-pattern}), and the associated code generation details in the \texttt{rain-llvm} module (sections \ref{sec:compiling-basic}, \ref{sec:compiling-cellular}, \ref{sec:compiling-heap}).

\tableofcontents

\newpage

\section{Basic Substructural Typing and Lifetimes}

\label{sec:basic}

\subsection{Motivation}

\rain is conceptually a purely functional language, and yet we want to be able to effect the world and manage resources in a manner low-level enough for \rain to be suitable as a backend for C-like languages. Monads, therefore, were not going to do. Taking inspiration from \cite{changetheworld}, we decided to implement both side-effects and resource management using a \textbf{substructural type system} to restrict the set of compile-able programs. Programs which do not obey the substructural type system remain valid expressions and can be \textit{reasoned} about (and therefore used in proofs), but cannot be compiled: these programs are termed \textit{heap} (and are especially helpful in e.g. proving an implementation's equivalence to an abstract specification), in contrast to \textit{concrete} programs.

Going beyond the classical use of \textbf{affine types} for resource management and \textbf{relevant types} for side effects, we extend the affine component of our type system with a notion of \textbf{borrowing} inspired by Rust's lifetime system to yield a powerful substructural dependent type system uniquely suited to reasoning about low-level resurce management. By allowing the special-casing of certain complex usage patterns using abstract lifetime constraints and concurrent separation logic, we open the possibility of formally verifying the \unsafe components of realistic Rust programs, thereby extending the reach and efficiency of safe code while obtaining \textit{better} performance than equivalent \unsafe code (as the compiler is now given more information).

\subsection{Substructural Types}

For the purposes of this section, we consider a \rain program's dependency DAG. Individual nodes may sometimes be marked \textbf{branching}, in which case all non-type dependencies are considered in a separate branch rooted at the node, called the \textbf{branchpoint}.

\begin{definition}[Consume]
    Any usage of a \rain-value may be marked as an \textbf{affine consumption} (or \textbf{move}), \textbf{relevant consumption}, and/or \textbf{temporal consumption}. If a usage of an affine \rain-value is a non-affine consumption, or a usage of a relevant \rain-value is an irrelevant (non-relevant) consumption, we call such a consumption \textbf{partial}, otherwise, it is \textbf{complete}. Generally, a \textbf{consumption} (unspecified) is assumed to be complete unless clearly determined otherwise. The affinity of a consumption of a non-affine value, and the relevance of a consumption of an irrelevant value, are ignored, and a program with or without them is equivalent. The user of the value is called the \textbf{consumer}.
\end{definition}

Consumption can therefore be represented using a \uint{8}, with the upper 5 bits reserved for future use and the bottom 3 representing affine/relevant/temporal flags. This is the \texttt{ConsumerFlags} struct, a value of which is associated to every dependency of an implementer of the \texttt{ValId} trait through the \texttt{consumer\_flags} method. Consumption is used to model both substructural typing and lifetimes, through use of the following definitions:

\begin{definition}[Borrow]
    A \rain value may \textbf{borrow} from one or more \rain values, it's (the \textbf{borrower}'s) \textbf{lenders}. A temporal edge is drawn from the borrower to any temporal consumers of it's lenders: a concrete \rain program must admit a topological sort respecting these temporal edges (i.e. remain a DAG with these temporal edges added).
\end{definition}

As an example of this concept, consider the following simple code:
\begin{center}
    \captionsetup{type=figure}
    \lstinputlisting[language=Rust]{code/example1.rs}
    \captionof{figure}{
        A simple \rain program in pseudo-Rust syntax
    }
    \label{fig:ex1code}
\end{center}
Let's convert this to a \rain-graph without lifetimes using the graphical representation used by this paper:
\begin{center}
    \captionsetup{type=figure}
    \ctikzfig{graphs/example1-direct}
    \caption{
        The program in Figure~\ref{fig:ex1code} as a \rain-graph.
        The nodes of a \rain-graph represent values, while the edges represent dependencies between the values.\textbf{Bold} edges represent consuming uses.
    }
    \label{fig:ex1dir}
\end{center}

Let's now consider how we can use the concept of borrowing to understand the program in Figure~\ref{fig:ex1dir}. In memory, the value \(s\) is marked as borrowing from the parameter \(v\), since it is a subslice of \(v\). More interestingly, the variable \(c\) is \textit{also} marked as borrowing from \(v\), since it depends on \(s\) and therefore must be computed while \(s\) still points to valid memory: this is called a \textbf{transient} borrow, as uses of \(c\) will not borrow \(v\) unless they do so independently. Hence, both \(c\) and \(s\) have temporal links to the consumer of \(v\), namely \(2v\). We hence obtain the annotated graph below:

\begin{center}
    \captionsetup{type=figure}
    \ctikzfig{graphs/example1-full}
    \caption{
        The program in Figure~\ref{fig:ex1code} as a \rain-graph,
        fully annotated with temporal edges (\textbf{dotted, grey} lines), borrows (\textbf{blue} lines), and transient borrows (\textbf{dark blue} lines).
    }
    \label{fig:ex1full}
\end{center}

\begin{definition}[Affine]
    An \textbf{affine} value can be affinely-consumed \emph{at most} once per branch in a concrete \rain program: these unique consumers are called \textbf{owners}, and a consumed vaue \textbf{owned}. A type is called an \textbf{affine type} if it's values may be affine. A usage of an affine value in the definition of a value does not necessarily need to consume it, but no uses of an affine value can be scheduled after a use which does consume it.
\end{definition}

An affine type in \rain corresponds to a non-\texttt{Copy} type in Rust, e.g. the \texttt{Vec} in Figure~\ref{fig:ex1code}. The affinity restriction prevents invalid code like the following:

\begin{center}
    \captionsetup{type=figure}
    \lstinputlisting[language=Rust]{code/example-double-use.rs}
    \captionof{figure}{
        An invalid \rain program displaying use-after-free
    }
    \label{fig:double-use-code}
\end{center}

When converted to a graph, we can clearly see there are \textit{two} consuming edges coming from \(v\), violating the affinity property described above

\begin{center}
    \captionsetup{type=figure}
    \ctikzfig{graphs/example-double-use}
    \caption{
        The \textbf{invalid} code in Figure~\ref{fig:double-use-code} represented as a \rain-graph
    }
    \label{fig:double-use-graph}
\end{center}

On the other hand, valid, branching code like the following is allowed:

\begin{center}
    \captionsetup{type=figure}
    \lstinputlisting[language=Rust]{code/example-branched.rs}
    \captionof{figure}{
        A valid \rain program displaying affine usage behind a branch
    }
    \label{fig:branched-code}
\end{center}

This is because, while there are still two consuming edges coming from \(v\), they target different branches, since the boolean \(\gamma\)-node (representing an \texttt{if}-statement) is considered branching:

\begin{center}
    \captionsetup{type=figure}
    \ctikzfig{graphs/example-branched}
    \caption{
        The code in Figure~\ref{fig:branched-code} represented as a \rain-graph
    }
    \label{fig:branched-graph}
\end{center}


\begin{definition}[Relevant]
    A \textbf{relevant} value \emph{must} be relevantly-consumed \emph{at least} once per branch in a concrete \rain program. A type is called a \textbf{relevant type} if it's values may be relevant.
\end{definition}

\begin{definition}[Linear/Substructural]
    A value or type which is \emph{both} affine \emph{and} relevant, i.e. must be consumed \emph{exactly} once per branch, is called \textbf{linear}. A value which is \emph{either} affine \emph{or} relevant is called \textbf{strongly substructural}. A value which borrows from another value but is not affine or relevant is called \textbf{(weakly) substructural}.
\end{definition}

Note that Rust does \textit{not}, at least directly, have linear or relevant types. Originally, it was assumed that safe code would not leak destructors, hence e.g. marking \texttt{std::mem::forget} as \unsafe. In this way, all affine types were considered, in a sense, linear as well (since they would be consumed by their \texttt{Drop} implementation if not consumed by a function). Unfortunately, however, it's pretty easy to violate this assumption using, e.g., interior mutability to create cycles in a reference counted type, as in Figure~\ref{fig:cycle-leak-code}. In that way, while \texttt{Vec}, for example, is affine, it is \textit{not} truly linear, even considering \texttt{Drop}. The same goes for, e.g., \texttt{Result}, which is ``pseudo-relevant'' as a warning is issued when it is left unused, and yet this can (intentionally) easily be circumvented via a statement of the form \texttt{let \_ = result;}.

\begin{figure}
    \lstinputlisting[language=Rust]{code/example-cycle-leak.rs}
    \captionof{figure}{
        A Rust program displaying an \texttt{Rc}-cycle causing a \texttt{Drop} leak.
    }
    \label{fig:cycle-leak-code}
\end{figure}

Rust, however, \textit{indirectly} supports relevant and linear values through \textit{side-effects} which cannot be ignored. As an example, consider the following Rust program, which consumes some user input:
\begin{center}
    \captionsetup{type=figure}
    TODO: this
    %\lstinputlisting[language=Rust]{code/example-input.rs}
    \captionof{figure}{
        A Rust program which accepts user input
    }
    \label{fig:example-input}
\end{center}
Since \rain is purely functional, we need to write the stream of user input as an explicit argument to the function. Similarly, we need to write the \textit{action} of \textit{consuming} user input, advancing the buffer, as an explicit \textit{result} of the function. The easiest way to do this is to treat a stream of input as a linear type, yielding the following program.
\begin{center}
    \captionsetup{type=figure}
    TODO: this
    %\lstinputlisting[language=Rust]{code/example-input-pure.rs}
    \captionof{figure}{
        A Rust program which accepts user input, rendered pure using relevant types
    }
    \label{fig:example-input-pure}
\end{center}
The thing is, without relevant types (i.e., if the input stream was just affine, like a non-\texttt{Copy} Rust type), we could write the naive but invalid program
\begin{center}
    \captionsetup{type=figure}
    TODO: this
    %\lstinputlisting[language=Rust]{code/example-input-ghost.rs}
    \captionof{figure}{
        A Rust program which accepts user input, \textbf{incorrectly} rendered pure
    }
    \label{fig:example-input-ghost}
\end{center}
This program is wrong since, though it correctly depends on user input, it never forwards the effect of actually consuming the input. If the input was consumed using a system call, another call to this program could occur
By making the action of consuming user input represented by a \textit{relevant} type, we are forced to pass it forwards when it happens. Now, affinity prevents this from being \textit{too} big a deal, since such a function would be useless, as \textit{nobody else} can interact with the stream once this function has consumed it. In other words, we've performed the equivalent of the \texttt{close} system call, except we didn't actually close the stream (and hence, may leak system resources). So this is not a soundness bug, just a resource leak.

That said, it turns out relevant types are \textit{essential} for IO (not just because files are much more valuable resource-wise than memory), versus for standard memory resources (where affine types are the bare minimum for correctness). To see why, we need to introduce the concept of \textit{state splitting}.
\begin{center}
    TODO: all this...
\end{center}
To see more examples of how I/O can be performed using relevant types, see subsection~\ref{subsec:io}

\textit{Exceptions} are also another example of relevant types in Rust. Consider, for example, the following Rust program, which will panic depending on the result of some check:
\begin{center}
    \captionsetup{type=figure}
    \lstinputlisting[language=Rust]{code/example-unwrap.rs}
    \captionof{figure}{
        A Rust program which may \texttt{panic!} if \texttt{compute} does not behave as expected.
    }
    \label{fig:example-unwrap}
\end{center}
In Rust, the return type of this program is simply \texttt{Data}, but, again, \texttt{rain} is both purely functional \textit{and} strictly terminating (see how we handle general recursion in our \texttt{nontermination-impl} paper!), and therefore, \textit{all} side effects, even exceptions, must be represented in the return type (unlike, e.g., Haskell, in which non-terminating programs may use \texttt{error}). The natural return type here, then, would be, for some \texttt{Exception} type, \texttt{EResult<Data, Panic>}, where \texttt{EResult} is a \texttt{Result}-like type encapsulating potential special compilation for thrown exceptions (using normal \texttt{Result} could work here too, special-casing using \texttt{Panic}, but then a normal \(\gamma\) node would have to act as \texttt{try-catch}, which would be annoying). So, explicitly, we have the pseudo-Rust code
\begin{center}
    \captionsetup{type=figure}
    \lstinputlisting[language=Rust]{code/example-unwrap-pure.rs}
    \captionof{figure}{
        A \rain program converting the Rust program in Figure~\ref{fig:example-unwrap} to be pure.
    }
    \label{fig:example-unwrap-pure}
\end{center}
We can draw this as a \rain graph as follows:
\begin{center}
    \captionsetup{type=figure}
    \ctikzfig{graphs/example-unwrap}
    \caption{
        The code in Figure~\ref{fig:example-unwrap-pure} represented as a \rain-graph
    }
    \label{fig:unwrap-graph}
\end{center}

The reason this is mentioned is because, in Rust, the \texttt{Exception} we raised can be considered to be a linear type. That is, Rust's syntax does \textit{not} provide a way to simply ``ignore'' the exception branch: it must be either returned from each function potentially invoking it or explicitly caught with \texttt{std::catch\_unwind}, and the latter may even fail in the case where \texttt{panic!} is allowed to \texttt{abort} the program.
In other words, exceptions are relevant. Since there's no way to \texttt{Copy} them either, they are also affine, and therefore linear. For more on exception handling, see subection~\ref{subsec:exceptions}.

This completes our description of the substructural component of \rain's type system: we now turn to the lifetime system, which works to programatically determine where borrow-edges must be inserted for a program to have sound low-level behaviour, again using a set of well-chosen compiler primitives.

\subsection{Lifetimes}

\subsection{Direct Borrows}

\subsection{Lifetime Parameters}

\subsection{Dependent Lifetimes}

\newpage

\section{Basic Implementation}

\label{sec:basic-impl}

\subsection{The \texttt{Group} and \texttt{Lifetime} structs}

\subsection{The \texttt{LifetimeGraph} and \texttt{LifetimeCtx} structs}

\subsection{\texttt{LifetimeCtx} construction}

\subsection{Substructural Checking}

\subsection{Borrow Checking}

\subsection{The \texttt{ResultLifetime} struct}

\subsection{Lifetimes and \texttt{Apply}}

\subsection{Borrowing from a \texttt{Parameter}}

\newpage

\section{Representing Basic Lifetime Patterns}

\label{sec:basic-pattern}

While the basic theory of lifetimes described above does not even support field lifetimes yet, it is nevertheless capable of representing many complex and useful programming patterns with powerful zero-cost abstractions. Here we will describe some of these fundamental patterns and their uses.

\subsection{Allocation}

\subsection{Splines and Mutability}

\subsection{Exceptions and Crashes}

\label{subsec:exceptions}

Using \rain's support of relevant dependent types, a robust exception handling system can be defined using purely functional terms composed of relevantly typed compiler builtins. As an example of this, we will begin by considering the following two example functions:
\begin{center}
    \captionsetup{type=figure}
    \lstinputlisting[language=Rust]{code/example-error-monad.rs}
    \captionof{figure}{
        Some Rust functions which make use of panics
    }
    \label{fig:example-error-monad}
\end{center}
We will first compile these using the \texttt{EResult} monad, a compiler-provided monad with special handling. We will then show how exceptions handling can be performed \textit{without} monads, which is a little more complicated since exceptions can crash the program (unlike exception-free IO, which can be blocking, introducing it's own issues), which is why we do not start with the monad-free version like in subsection~\ref{subsec:io}

\subsubsection{Exceptions with Monads}

To translate the first example into a pure program, we can use the monadic \(\mbind\) operator (written \texttt{bind}), which has the standard type
\begin{equation}
    (\mbind)(A, B): \EResult(A, E) \to (A \to \EResult(B, E)) \to \EResult(B, E)
\end{equation}
\begin{center}
    \captionsetup{type=figure}
    \lstinputlisting[language=Rust]{code/example-panic-monad-pure.rs}
    \captionof{figure}{
        The function \texttt{square\_squared} from Figure~\ref{fig:example-error-monad} rendered pure through use of \(\mbind\) (written \texttt{bind})
    }
    \label{fig:example-panic-monad-pure}
\end{center}
Which yields the following \rain graph:
\begin{center}
    \captionsetup{type=figure}
    TODO: this
    \captionof{figure}{
        The program in Figure~\ref{fig:example-panic-monad-pure} as a \rain graph
    }
    \label{fig:panic-monad-graph}
\end{center}
To translate the second example in Figure~\ref{fig:example-error-monad} into a pure program, we need to type \texttt{catch\_unwind}. Since a \texttt{panic!} may yield an \texttt{abort}, we can't just escape the \(\EResult\) monad, and so instead must declare a term of type
\begin{equation}
    \mathtt{catch\_unwind}(T): \EResult(T, \Panic) \to \EResult(T + \Exception, \Abort)
\end{equation}
We'll also use the standard
\begin{equation}
    \mathtt{fmap}(A, B): \EResult(A, E) \to \EResult(B, E)
\end{equation}
which can be defined as usual via
\begin{equation}
    \mathtt{fmap} \ f \ e = e \mbind (\mathtt{ret} \circ f)
\end{equation}
if desired. Using this, we obtain the following program:
\begin{center}
    \captionsetup{type=figure}
    \lstinputlisting[language=Rust]{code/example-catch.rs}
    \captionof{figure}{
        The function \texttt{checked\_square} from Figure~\ref{fig:example-error-monad} rendered pure through use of \(\mathtt{catch\_unwind}\) and \(\mathtt{fmap}\))
    }
    \label{fig:example-catch-pure}
\end{center}
Which yields the following \rain graph:
\begin{center}
    \captionsetup{type=figure}
    TODO: this
    \captionof{figure}{
        The program in Figure~\ref{fig:example-catch-pure} as a \rain graph
    }
    \label{fig:catch-graph}
\end{center}
Note that here a program being \texttt{abort}ed is \textit{also} considered a relevant effect, so escaping the \texttt{EResult} is only possible using a proof that \texttt{panic!} will not abort in the current configuration, as follows: given a configuration term
\begin{equation}
    \mathtt{no\_abort}: \Option(\Panic \to \Exception)
\end{equation}
and using
\begin{equation}
    \mathtt{EResult::map\_left}(f): \EResult(T, E) \to \EResult(T, f(E))
\end{equation}
and
\begin{equation}
    \mathtt{catch\_exception}(T): \EResult(T, \Exception) \to \Result(T, \Exception)
\end{equation}
we can rewrite the second program to the following, having a stronger type:
\begin{center}
    \captionsetup{type=figure}
    TODO: this
    %\lstinputlisting[language=Rust]{code/example-catch-strong.rs}
    \captionof{figure}{
        The program in Figure~\ref{fig:example-catch-pure} with it's type strengthened using the \(\mathtt{no\_assert}\) compile time configuration switch.
    }
    \label{fig:example-catch-strong}
\end{center}
Yielding graph
\begin{center}
    \captionsetup{type=figure}
    TODO: this
    \captionof{figure}{
        The program in Figure~\ref{fig:example-catch-strong} as a \rain graph
    }
    \label{fig:catch-strong-graph}
\end{center}
Given a compile-time checked assumption
\begin{equation}
    \mathtt{no\_abort\_sure}: \sum_{p: \Panic \to \Exception} \mathtt{no\_abort} = \Some(p)
\end{equation}
We obtain a proof by transport over the LHS of \(\typeof(\mathtt{checked\_square})\)
\begin{equation}
    \typeof(\mathtt{checked\_square}) = \mathtt{Data} \times \mathtt{Data} \to \mathtt{Data}
\end{equation}
which allows us to transport \(\mathtt{checked\_square}\) to a pure function, the exceptions within successfully encapsulated.
Note in actuality we must often use a more conceptually complex solution to support dependent typing in the general case, but the essence of the solution is the same as above (and the above is perfectly valid for manual rewriting, or for code-generation if it is detected that \texttt{square} is not of dependent type). For that, see Section \ref{subsec:exceptions}.

\subsubsection{Exceptions without Monads}

\subsection{IO}

\label{subsec:io}

\subsubsection{``Simple'' IO}

\subsubsection{IO with Monads}

\subsubsection{IO with Exceptions}

\subsubsection{Blocking IO}

\subsubsection{Event-Driven IO}

\subsection{Time and Execution}

\subsubsection{Real-time computing}

\subsubsection{Strict vs. Lazy semantics}

\newpage

\section{Field Lifetimes}

\label{sec:field}

\subsection{Motivation}

\subsection{Abstract Nodes and Edge Forwarding}

\subsection{Affine Field Lifetimes}

\subsection{Field Borrows}

\subsection{Relevant Field Lifetimes}

\newpage

\section{Field Lifetime Implementation}

\label{sec:field-impl}

\subsection{Extending the \texttt{nodes} table}

\subsection{Implementing edge forwarding}

\subsection{Extending the \texttt{Branch} struct and shadow-check}

\newpage

\section{Representing Field Lifetime Patterns}

\label{sec:field-pattern}

\subsection{Field splines and mutability}

\subsection{Commutative monad stacks}

\newpage

\section{Compiling Basic Lifetime Patterns}

\label{sec:compiling-basic}

\subsection{SROA, \texttt{mem2reg}, and \texttt{memcpy}}

\subsection{Affine projections}

\subsection{Field lifetimes through function calls}

\newpage

\section{Cellular Lifetimes and Advanced State Splitting}

\label{sec:cellular}

\subsection{Motivation}

\subsection{Split/Join}

\subsection{Drop}

\subsection{Send + Sync}

\subsection{Negative Dependency Conditions}

\section{Implementing Cellular Lifetimes}

\label{sec:cellular-impl}

\newpage

\section{Representing Cellular Lifetime Patterns}

\label{sec:cellular-pattern}

\subsection{Advanced Splines}

\subsection{Cellular Field Borrows}

\subsection{Atomic Operations}

\subsection{\texttt{UnsafeCell} sans \unsafe}

\newpage

\section{Compiling Cellular Lifetimes}

\label{sec:compiling-cellular}

\newpage

\section{Abstract Heaps}

\label{sec:heap}

\newpage

\section{Implementing Abstract Heaps}

\label{sec:heap-impl}

\newpage

\section{Representing Abstract Heap Patterns}

\label{sec:heap-pattern}

\newpage

\section{Compiling Abstract Heap Patterns}

\label{sec:compiling-heap}

\newpage

\section{Future Directions}

\newpage

\printbibliography

\end{document}